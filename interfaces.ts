export interface post {
  fields: {
    authors: author[];
    title: string;
    category?: category;
    description: string;
    slug: string;
    date: string;
    updatedDate: string;
    body: string;
    tags: string[];
    image?: contentfulImage;
    relatedPosts?: post[];
    externalUrl?: string;
  }
}

export interface contentfulImage {
  fields: {
    title: string
    file: {
      url: string;
    }
  }
}

export interface author {
  fields: {
    name: string;
    gitLabHandle: string;
    headshot: {
      fields: {
        title: string;
        description: string;
        file: {
          url: string;
        };
      };
    };
  };
}

export interface quote {
  fields: {
    authorCompany: string;
    authorName: string;
    authorRole: string;
    image: contentfulImage;
    text: string;
    variant: string;
  }
}

export interface category {
  fields: {
    name: string;
    slug: string;
  }
}

export interface categoryGroup {
  category: category;
  posts: post[];
}

export interface breadcrumb {
  title: string;
  href?: string;
  data_ga_name?: string;
  data_ga_location?: string;
}

export interface cta {
  fields: {
    header?: string;
    description: string;
    button: CTAbutton
  }
}

export interface CTAbutton {
  fields: {
    text: string;
    variant: string;
    iconName: string;
    iconVariant: string;
    href: string;
  }
}