import { createClient } from "contentful";
import { CONFIDENTIAL_TAG, LOCALES } from "../common/constants.ts";
import { getNavigationClient } from "../plugins/contentful";
import { convertToCaseSensitiveLocale } from "../common/util";

if (process.env.CTF_ENVIRONMENT) {
  console.log(`Using contentful environment ${process.env.CTF_ENVIRONMENT}`)
}

const client = createClient({
  space: process.env.CTF_SPACE_ID,
  accessToken: process.env.CTF_CDA_ACCESS_TOKEN,
  environment: process.env.CTF_ENVIRONMENT || 'master',
  host: "cdn.contentful.com",
  logHandler: (level, data) => {
    if (data.includes('Rate limit')) {
      return;
    }
  }
});

export async function fetchCategoryRoutes(id) {
  try {
    let skip = 0;
    const limit = 100;
    let total = 0;
    let allEntries = [];
    const options = {
      content_type: "category",
      select: "fields.slug",
      limit,
      skip,
    }
    if (id) {
      options['sys.id'] = id
    }

    let entries = await client.getEntries(options);

    if (entries.items.length) {
      allEntries = allEntries.concat(entries.items);
      total = entries.total;

      while (allEntries.length < total) {
        skip += limit;
        entries = await client.getEntries({
          content_type: "category",
          select: "fields.slug",
          limit,
          skip,
        });
        allEntries = allEntries.concat(entries.items);
      }
      return allEntries.map((entry) => `/categories/${entry.fields.slug}`);
    }
    return [];
  } catch (error) {
    console.error("Error fetching category routes from Contentful:", error);
    throw error;
  }
}

export function fetchTagRoutes() {

  const validTags = [
    "agile",
    "AI/ML",
    "AWS",
    "bug bounty",
    "careers",
    "CI",
    "CD",
    "CI/CD",
    "cloud native",
    "code review",
    "collaboration",
    "community",
    "contributors",
    "customers",
    "demo",
    "design",
    "developer survey",
    "DevOps",
    "DevOps platform",
    "DevSecOps",
    "DevSecOps platform",
    "events",
    "features",
    "frontend",
    "Group Conversations",
    "git",
    "GitOps",
    "GKE",
    "google",
    "growth",
    "inside GitLab",
    "integrations",
    "kubernetes",
    "news",
    "open source",
    "partners",
    "patch releases",
    "performance",
    "product",
    "production",
    "releases",
    "remote work",
    "research",
    "security",
    "security releases",
    "security research",
    "solutions architecture",
    "startups",
    "testing",
    "tutorial",
    "UI",
    "user stories",
    "UX",
    "webcast",
    "workflow",
    "zero trust",
    "automotive", 
    "embedded development", 
    "public sector", 
    "education", 
    "financial services"
  ]
  return validTags.map((tag) => `/tags/${tag.replace('/', '_').replace(' ', '-')}`);

}

export async function fetchPostRoutes(id) {
  try {
    let skip = 0;
    const limit = 100;
    const EXCLUSION_TOKEN = '#'
    let total = 0;
    let allEntries = [];
    const options = {
      content_type: "blogPost",
      select: "fields.slug, fields.date, fields.title, fields.body",
      limit,
      skip,
      'metadata.tags.sys.id[nin]': CONFIDENTIAL_TAG, // Exclude entries with the Confidential tag
      "fields.externalUrl[exists]": false // Exclude entries with text in the externalUrl field
    }
    if (id) {
      options['sys.id'] = id
    }
    let entries = await client.withAllLocales.getEntries(options);
    if (entries.items.length) {
      allEntries = allEntries.concat(entries.items);
      total = entries.total;

      while (allEntries.length < total) {
        options.skip += limit;
        entries = await client.withAllLocales.getEntries(options);
        allEntries = allEntries.concat(entries.items);
      }
      const baseSlugs = allEntries.filter(entry => entry.fields.body['en-US'] !== EXCLUSION_TOKEN).map(
        (entry) => `/${entry.fields.date['en-US'].replaceAll("-", "/")}/${entry.fields.slug['en-US']}/`,
      );
      const localizedSlugs = [];
      allEntries.forEach((entry) => {
        const locales = Object.keys(entry.fields.title);
        locales.forEach((locale) => {
          if (locale !== 'en-US') {
            localizedSlugs.push(
              `/${locale.toLowerCase()}/${entry.fields.date['en-US'].replaceAll("-", "/")}/${entry.fields.slug['en-US']}/`,
            );
          }
        });
      });
      return [...baseSlugs, ...localizedSlugs];
    }
    return [];
  } catch (error) {
    console.error("Error fetching post routes from Contentful:", error);
    throw error;
  }
}

export async function fetchAuthorsRoutes(id) {
  try {
    let skip = 0;
    const limit = 100;
    let total = 0;
    let allEntries = [];
    const options = {
      content_type: "author",
      select: "sys.id",
      limit,
      skip,
    }
    if (id) {
      options['sys.id'] = id
    }
    let entries = await client.getEntries(options);

    if (entries.items.length) {
      allEntries = allEntries.concat(entries.items);
      total = entries.total;

      while (allEntries.length < total) {
        skip += limit;
        entries = await client.getEntries({
          content_type: "author",
          select: "sys.id",
          limit,
          skip,
        });
        allEntries = allEntries.concat(entries.items);
      }
      return allEntries.map((entry) => `/authors/${entry.sys.id}`);
    }
    return [];
  } catch (error) {
    console.error("Error fetching author routes from Contentful:", error);
    throw error;
  }
}

/**
 * Return the navigationData needed for the Nav and the footer separated by each locale the application supports.
 * Note: When the A/B navigation is not needed anymore, please remove it from here and adjust the code
 * @returns {Promise<T>}
 */
export async function getAllLocalesNavData() {
  const ctfConfig = {
    // Order: Nav, Footer
    'sys.id[in]':
      '4ANorm7zz94uE5N6oexbdi,QxdoqHd5ixKTwFjIHYvYI',
    order: 'sys.id',
    include: 10,
  };

  const localePromises = Object.values(LOCALES).map((locale) =>
    getNavigationClient().getEntries({
      ...ctfConfig,
      locale: convertToCaseSensitiveLocale(locale),
    }),
  );

  const response = await Promise.all(localePromises);

  // This function creates an object with each parameter being a locale
  return response.reduce((acumObj, { items }) => {
    const [navigation, footer] = items;
    const locale = navigation.sys.locale;
    acumObj[locale.toLowerCase()] = { navigation, footer };
    return acumObj;
  }, {});
}
