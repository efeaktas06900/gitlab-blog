const fs = require('fs');
const path = require('path');

const filePath = path.join(__dirname, '..', 'dist', 'sitemap.xml');

fs.readFile(filePath, 'utf8', (err, data) => {
    if (err) {
        console.error('Error reading sitemap.xml:', err);
        process.exit(1);
    }

    // Regular expression to replace /blog/<locale>/ with /<locale>/blog/
    const updatedData = data
        .replace(/<loc>https:\/\/about\.gitlab\.com\/blog\/([a-z]{2}-[a-z]{2})\//g, '<loc>https://about.gitlab.com/$1/blog/')
        .replace(/<loc>https:\/\/about\.gitlab\.com\/blog\//g, '<loc>https://about.gitlab.com/blog/'); // Keeps default routes unchanged

    fs.writeFile(filePath, updatedData, 'utf8', (err) => {
        if (err) {
            console.error('Error writing sitemap.xml:', err);
            process.exit(1);
        }

        console.log('sitemap.xml updated successfully');
    });
});