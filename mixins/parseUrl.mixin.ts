import Vue from 'vue'
import {post} from '../interfaces'
import {LOCALES} from "~/common/constants";

export const ParseUrlMixin = Vue.extend({
  methods:{
    parseUrl(post: post, locale?: string){
      if(!post || !post.fields.date){
        return ''
      }

      const urlLocale = locale && LOCALES.DEFAULT !== locale ? `/${locale}` : '';

      return post.fields.externalUrl || `${urlLocale}/blog/${post.fields.date.replaceAll('-','/')}/${post.fields.slug}/`
    }
  }
})