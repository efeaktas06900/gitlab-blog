import Vue from 'vue'

export const PreviewMixin = Vue.extend({
    data(){
        return {
            livePreviewSubscriptions: []
        }
    },
    beforeDestroy() {
        this.livePreviewSubscriptions.forEach((unsubscribe: any) => unsubscribe())
    },
    computed:{
        isPreview(){
            return !!this.$route.query['preview']
        }
    }
})