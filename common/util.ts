/**
 * Converts a locale code to a case sensitive locale code (i.e en-us to en-US)
 * @param localeCode
 */
export function convertToCaseSensitiveLocale(localeCode: string) {
  const parts = localeCode.split('-');

  if (parts.length === 2) {
    return `${parts[0].toLowerCase()}-${parts[1].toUpperCase()}`;
  }

  return localeCode.toLowerCase();
}