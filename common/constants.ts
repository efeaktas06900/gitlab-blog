export const CONFIDENTIAL_TAG = 'confidential';

export const LOCALES = Object.freeze({
    DEFAULT: 'en-us',
    GERMAN: 'de-de',
    FRENCH: 'fr-fr',
    JAPANESE: 'ja-jp',
});
