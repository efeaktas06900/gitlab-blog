# GitLab Blog

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```


## Contentful 

To enable local development with your Contenful space, you will need to add a .env file with your space ID and access token. Please ensure this file in your .gitignore and not committed.

You can access these keys in Contentfulfrom the Blog space. In the upper right click settings -> API Keys

```
CTF_SPACE_ID=REPLACE-WTH-ID
CTF_CDA_ACCESS_TOKEN=REPLACE-WTH-TOKEN
CTF_LIVE_PREVIEW=active
CTF_CONTENT_PREVIEW_ACCESS_TOKEN=REPLACE-WTH-TOKEN
CTF_NAV_SPACE_ID=REPLACE-WTH-ID
CTF_NAV_PREVIEW_ACCESS_TOKEN=REPLACE-WTH-TOKEN
CTF_NAV_CDA_ACCESS_TOKEN=REPLACE-WTH-TOKEN
```

## Slippers

This project relies on Slippers, the open-source GitLab Marketing Web Design System, as a key dependency. Slippers provides essential components like Typography, Buttons, and containers, ensuring consistency in styling with the main site.

https://gitlab.com/gitlab-com/marketing/digital-experience/slippers-ui 

## Special Directories

You can create the following extra directories, some of which have special behaviors. Only `pages` is required; you can delete them if you don't want to use their functionality.

### `assets`

The assets directory contains your uncompiled assets such as Stylus or Sass files, images, or fonts.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/assets).

### `components`

The components directory contains your Vue.js components. Components make up the different parts of your page and can be reused and imported into your pages, layouts and even other components.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/components).

### `layouts`

Layouts are a great help when you want to change the look and feel of your Nuxt app, whether you want to include a sidebar or have distinct layouts for mobile and desktop.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/layouts).


### `pages`

This directory contains your application views and routes. Nuxt will read all the `*.vue` files inside this directory and setup Vue Router automatically.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/get-started/routing).

### `plugins`

The plugins directory contains JavaScript plugins that you want to run before instantiating the root Vue.js Application. This is the place to add Vue plugins and to inject functions or constants. Every time you need to use `Vue.use()`, you should create a file in `plugins/` and add its path to plugins in `nuxt.config.js`.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/plugins).

### `static`

This directory contains your static files. Each file inside this directory is mapped to `/`.

Example: `/static/robots.txt` is mapped as `/robots.txt`.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/static).

## Deployment

The blog is deployed separately from our other projects and is pushed to it's own folder in our cloud bucket, this also means the job to delete old files must be separate and check for only blog files.

For this a failsafe was implemented that checks how many pages this delete job will delete, and abort it if it exceeds a set threshold of 15 pages, however it is possible to bypass this test in case of intentionally deleting more pages than the threshold allows for, to do this, the delete job must be run with the flag `FORCE_DELETE` as true (it is very important to make sure this flag is not left enabled after manually running the delete job).

### Localized blog files

The localized blog files need to live in the root folder in our cloud bucket since all of our localized URLS have the same structure `about.gitlab.com/LOCALE_CODE/PAGE_URL`. for this reason they are copied over from our built files into the root folder, and excluded from the delete job in our other repositories.

However, when running in your local environment, these files are still generated to `/blog/LOCALE_CODE/BLOG_URL` as they are only moved during deployment.
